<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ HEADER _______________________ -->

<div id="topbar"> </div>

	<div id="headermenu">

	<div class="header-menu-inner">
	<div id="logo"><a href="<?php print $GLOBALS['base_url']; ?>/"><img src="<?php print $logo; ?>"/></a></div>

	<div id="menu-usuari">
		<div id="credits-user"><?php print render($page['saldo']); ?> </div>
		<div id="id-user"><?php print render($page['userid']); ?></div>
		<div id="options-user">
		<?php print render(siteoverride_embed_block('menu', 'menu-men-membre')); ?>
		</div>

	</div> <!--menu usuari-->
	</div>
	</div>
</div>



<div id="header_message">
	<div id="header_message_inner">
	<?php print render($page['menuprincipal']); ?>
	</div>
</div>
<div id="main-content">
	<div id="main-content-inner">

		<div id="main-top">

		<?php if ($page['sidebar_first']): ?>
		<div id="sidebar">
			<div class="sidebar-inner">
			<?php print render($page['sidebar_first']); ?>
			</div>
		</div>
		<?php endif; ?>

		<div id="content">
		<h1><?php print $title; ?></h1>

		<?php print render($page['content']); ?>

		</div>

	</div>
</div>



<div id="pre-footer">

	<div id="pre-footer-top">
		<div id="column-footer">
			<div class="main-inner">
			<h2>La xarxa d'intercanvi</h2>
			<ul>
			  <li><a href="#">Què és Eco Alt Congost</a></li>
			  <li><a href="#">Moneda complementària</a></li>
			  <li><a href="#">Com participar</a></li>
			  <li><a href="#">Preguntes freqüents</a></li>
			</ul>

			</div>
		</div>
		<div id="column-footer">
			<div class="main-inner">
			<h2>Ofertes</h2>
			<ul>
			  <li><a href="#">Productes</a></li>
			  <li><a href="#">Serveis i Coneixements</a></li>
			  <li><a href="#">Espais i terres</a></li>
			  <li><a href="#">Compartir cotxe</a></li>
			</ul>
			</div>
		</div>
		<div id="column-footer">
			<div class="main-inner">
			<h2>Demandes</h2>
			<ul>
			  <li><a href="#">Productes</a></li>
			  <li><a href="#">Serveis i Coneixements</a></li>
			  <li><a href="#">Espais i terres</a></li>
			  <li><a href="#">Compartir cotxe</a></li>
			</ul>
			</div>
		</div>
		<div id="column-footer">
			<div class="main-inner">
			<h2>Comunitat</h2>
			<ul>
			  <li><a href="#">Membres de la xarxa</a></li>
			  <li><a href="#">Notícies</a></li>
			  <li><a href="#">Agenda</a></li>
			</ul>
			</div>
		</div>
		<div id="column-footer">
			<div class="main-inner no-padding-right">
			<h2>Xarxes socials</h2>
			<ul>
			  <li><a href="#">Facebook</a></li>
			  <li><a href="#">Twitter</a></li>
			  <li><a href="#">Llista RSS</a></li>
			</ul>
			</div>
		</div>
	</div>

</div>

<div id="footer">
<div id="footer-inner"> <ul><li><a href="#">Text Legal</a></li><a href="#">Normes de funcionament</a><li></li></ul>
</div>







</div> <!-- /page -->