<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ HEADER _______________________ -->

<div id="topbar"> </div>
<div id="headermenu">
   <div id="header_inner">
	<div class="headerprimary">
		<?php if ($main_menu || $secondary_menu): ?>
			<?php print theme('links', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('links', 'clearfix', 'main-menu')))); ?>
			<?php print theme('links', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary', 'class' => array('links', 'clearfix', 'sub-menu')))); ?>
      		<?php endif; ?>
	</div>
	<div class="headerusermenu">
	<div class="avatar"><img src="http://gidsy-static.s3.amazonaws.com/avatars/sydthekid/resized/60/IMG_0472.jpg"/> </div>
	<a href="<?php print $GLOBALS['base_url']; ?>/messages"><div class="missatges"><?php print privatemsg_unread_count(); ?></div></a>
	</div>
	<div class="headerbalance">
		<div id="credits-user"><?php print render($page['saldo']); ?> </div>
	</div>
   </div>
</div>
<div id="header_message_interior">
	<div id="header_message_inner">
		<h2 class="interior">Oferta · <?php print $node->field_advert_type['und']['0']['taxonomy_term']->name; ?> · <?php print $title; ?></h2>
		<div id="preu"><div class="preu-inner"><?php print $node->field_price_list['und'][0]['value']; ?> ECO </div></div>


		<div id="categoria">
			<div class="categoria-inner tipus<?php print $node->field_advert_type['und']['0']['taxonomy_term']->tid; ?>">
				<a href="#"><?php print $node->field_advert_cat['und']['0']['taxonomy_term']->name; ?></a>
			</div>
		</div>

	</div>
</div>
<div id="messages">
	<div class="messages_inner">
	<?php print $messages; ?>
	</div>
</div>

<div id="main-content">
	<div id="main-content-inner">

		<div id="main-top">

		<div id="sidebar">
			<div class="sidebar-inner">
			<?php print render($page['sidebar_first']); ?>
			</div>
		</div>

		<div id="content">
			<?php print render($page['content']); ?>
		</div>

	</div>
</div>



<div id="pre-footer">

	<div id="pre-footer-top">
		<div id="column-footer">
			<div class="main-inner">
			<h2>La xarxa d'intercanvi</h2>
			<ul>
			  <li><a href="#">Què és Eco Alt Congost</a></li>
			  <li><a href="#">Moneda complementària</a></li>
			  <li><a href="#">Com participar</a></li>
			  <li><a href="#">Preguntes freqüents</a></li>
			</ul>

			</div>
		</div>
		<div id="column-footer">
			<div class="main-inner">
			<h2>Ofertes</h2>
			<ul>
			  <li><a href="#">Productes</a></li>
			  <li><a href="#">Serveis i Coneixements</a></li>
			  <li><a href="#">Espais i terres</a></li>
			  <li><a href="#">Compartir cotxe</a></li>
			</ul>
			</div>
		</div>
		<div id="column-footer">
			<div class="main-inner">
			<h2>Demandes</h2>
			<ul>
			  <li><a href="#">Productes</a></li>
			  <li><a href="#">Serveis i Coneixements</a></li>
			  <li><a href="#">Espais i terres</a></li>
			  <li><a href="#">Compartir cotxe</a></li>
			</ul>
			</div>
		</div>
		<div id="column-footer">
			<div class="main-inner">
			<h2>Comunitat</h2>
			<ul>
			  <li><a href="#">Membres de la xarxa</a></li>
			  <li><a href="#">Notícies</a></li>
			  <li><a href="#">Agenda</a></li>
			</ul>
			</div>
		</div>
		<div id="column-footer">
			<div class="main-inner no-padding-right">
			<h2>Xarxes socials</h2>
			<ul>
			  <li><a href="#">Facebook</a></li>
			  <li><a href="#">Twitter</a></li>
			  <li><a href="#">Llista RSS</a></li>
			</ul>
			</div>
		</div>
	</div>

</div>

<div id="footer">
<div id="footer-inner"> <ul><li><a href="#">Text Legal</a></li><a href="#">Normes de funcionament</a><li></li></ul>
</div>







</div> <!-- /page -->