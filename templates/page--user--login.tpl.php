<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>


  <!-- ______________________ HEADER _______________________ -->

<div id="topbar"> </div>

	<div id="headermenu">

	<div class="header-menu-inner">
	<div id="logo"><a href="<?php print $GLOBALS['base_url']; ?>/"><img src="<?php print $logo; ?>"/></a></div>


	</div>
	</div>
</div>



<div id="header_message">
	<div id="header_message_inner">
	<?php print render($page['menuprincipal']); ?>
	</div>
</div>


<div id="main-content">
	<div id="main-content-inner">

		<div id="main-top">
		<?php print $messages; ?>
		<?php if ($page['sidebar_first']): ?>
		<div id="sidebar">
			<div class="sidebar-inner">
			<?php print render($page['sidebar_first']); ?>
			</div>
		</div>
		<?php endif; ?>
			<?php global $user; ?>
			<?php if ($user->uid) : ?>
			<?php else : ?>
		<div id="toboggan-right">
		<h1>Una nova moneda, moltes possibilitats.</h1>
		<ul>
		<li class="thick">Intercanvia serveis i coneixements</li>
		<li class="thick">Lloga, ven o compra productes</li>
		<li class="thick">Cedeix o aprofita espais i terres</li>
		<li class="thick">Comparteix cotxe amb els veïns i veïnes</li>
		<li class="thick">Participa en activitats comunitàries</li>
		<li class="euro"><b>I estalvia euros!</b> <a href="http://ecoaltcongost.org">Encara no estàs registrat/da?</a></li>
		</ul>


		<div class="mobile-toboggan">

		<img src="http://chart.apis.google.com/chart?cht=qr&chs=140x140&chld=L|0&chl=http%3A%2F%2Ffiles.appsgeyser.com%2Fecoaltcongost.apk"/>
		<a href="http://m.ecoaltcongost.org">Connecta't amb el teu mòbil</a> o <b><a href="http://www.appsgeyser.com/img/download_app_btn_small.png">descarrega't l'aplicació</a></b> escannejant el codi amb el teu smartphone Android!
		<br /><span style="font-size:12px;">Útil per a comerços i per a operar sense ordinador.</span>
		</div>

		</div>
		<?php endif; ?>
		<div id="content-toboggan">
		<h1><?php print $title;?></h1>

			<?php print render($page['content']); ?>
		</div>

	</div>


</div>

<?php global $user; ?>
<?php if ($user->uid) : ?>

<div id="pre-footer">


</div>
<?php endif; ?>
<div id="footer">
<div id="footer-inner"> <ul><li><a href="#">Text Legal</a></li><a href="#">Normes de funcionament</a><li></li></ul>
</div>







</div> <!-- /page -->