<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ HEADER _______________________ -->

<div id="topbar"> </div>

	<div id="headermenu">

	<div class="header-menu-inner">
	<div id="logo"><a href="<?php print $GLOBALS['base_url']; ?>/"><img src="<?php print $logo; ?>"/></a></div>

	<div id="menu-usuari">
	<?php global $user; ?>
	<?php if ($user->uid) : ?>
		<div id="credits-user"><?php print render($page['saldo']); ?> </div>

		<div id="id-user"><?php print render($page['userid']); ?></div>
		<div id="options-user">
		<?php print render($page['menumembre']); ?>
		</div>

	</div> <!--menu usuari-->

        <?php else : ?>

	<a id="botoaccedeix" href="<?php print $GLOBALS['base_url']; ?>/user/login">Accedeix</a>
	<?php endif; ?>
	</div>
	</div>
</div>



<div id="header_message">
	<div id="header_message_inner">
	<?php print render($page['menuprincipal']); ?>
	</div>
</div>

<div id="main-content">
	<div id="main-content-inner">

		<div id="main-top">

		<div id="sidebar">
			<div class="sidebar-inner">
			<?php print render($page['sidebar_first']); ?>
			</div>
		</div>

		<div id="content">
			<?php print render($page['content']); ?>
		</div>

	</div>
</div>



<div id="pre-footer">



</div>

<div id="footer">

	<div id="menucenterpage">

		<div class="menus-front">

	    <div style="padding-left:10px;padding-top:5px;"></div>

	    <div id="columna-llistat">
	    	<div class="columna-llistat-inner">
	    	<?php print render($page['menuserveis']); ?>
	    	</div>
	    </div>

	    <div id="columna-llistat">
	    	<div class="columna-llistat-inner">
	    	<?php print render($page['menuproductes']); ?>
	    	</div>
	    </div>


    	    <div id="columna-llistat">
	    	<div class="columna-llistat-inner">
	    	<?php print render($page['menuespais']); ?>
	    	<?php print render($page['menuterres']); ?>
	    	</div>
	    </div>


	    <div id="columna-llistat">
	    	<div class="columna-llistat-inner">
	    	<h2>Compartir cotxe des de</h2>
	    	<div class="item-list">
	    	<ul>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=88">Aiguafreda</a></li>
	        <li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=93">Balenyà</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=92">Centelles</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=89">Figaró-Montmany</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=90">Sant Martí de Centelles</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=94">Seva</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=91">Tagamanent</a></li>
	    	</ul>
	    	</div>

	    	<h2>Demandes</h2>
	    	<div class="item-list">
	    	<ul>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/demandes/serveis">Serveis i coneixements</a></li>
	        <li><a href="<?php print $GLOBALS['base_url']; ?>/demandes/productes">Productes</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/demandes/compartircotxe">Compartir cotxe</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/demandes/allotjamentiespais">Espais i allotjament</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/demandes/terres">Terres de cultiu</a></li>
	    	</ul>
	    	</div>

	    	</div>
	    </div>

	    </div>

	</div> <!-- menu centerpage -->







</div> <!-- /page -->