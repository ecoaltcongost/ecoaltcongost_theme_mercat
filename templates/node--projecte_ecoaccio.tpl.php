<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>">


	<div class="node-inner">

	<div id="anunci">

		<div id="anunci-right">
			<?php if ($content['field_image']) : ?>
			<div id="image-ad"><div class="glossy">
			<div class="morphing-tinting">
			<?php print render($content['field_image']); ?>
			</div>
			</div>
			<?php endif; ?>
			</div>

		</div>

		<div id="anunci-left">

			<h1><?php print $title; ?></h1>

			<div id="filariadna-ad"></div>

		        <div id="descripcio-ad">
			<?php print render($content['field_description']); ?>
		        </div>

		        <div id="descripcio-ad">
		        <h3>La teva recompensa</h3>
		        <div class="importeco">
				<?php print render($content['field_ecos']); ?>
			</div>
			<div id="reward-instructions">
			<?php print render($content['field_reward_instrucions']); ?>
			</div>
			</div>

			<div id="descripcio-ad">
		        <h3>Què aporta a la comunitat?</h3>

		        <?php print views_embed_view('project_type', 'block'); ?>

		        </div>



			<div id="author-box">

			<div id="author-ad">
				<?php print views_embed_view('autor_anunci', 'block'); ?>
			</div>

			<div id="author-contacte">
			<?php global $user; ?>
			<?php if ($user->uid) : ?>
			<div id="contacte-caixa">

				<div class="telefon">
				<?php
				$node_author = user_load($node->uid);
				if ($node_author->field_privacity_telefon['und'][0]['value']): ?>
				<?php print t('No visible'); ?>

				<?php else : ?>

				<?php
				$node_author = user_load($node->uid);
				print ($node_author->field_user_telefon['und'][0]['value']);
				?>

				<?php endif; ?>
				</div>

			<?php
				if ($url = privatemsg_get_link(array(user_load($node->uid)))) {
					print l(t('Contactar'), $url, array('attributes' => array('class' => 'botocomprar')));
				}
			?>
			</div>
			<?php endif; ?>
			</div>
			</div>

			<div id="comentaris">
			<?php print render($content['comments']); ?>
			</div>

		</div>

	</div>


	</div> <!-- /node-inner -->
</div> <!-- /node-->






