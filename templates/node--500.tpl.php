<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>">
	<div class="node-inner">

    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>

    <?php print $user_picture; ?>

    <?php if ($display_submitted): ?>
      <span class="submitted"><?php print $date; ?> — <?php print $name; ?></span>
    <?php endif; ?>

  	<div class="content">
  	  <?php
  	    // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        print render($content);
       ?>

       <div style="float:left; margin-right:10px;"><img src="/sites/default/files/print500.png" width="100px"/></div>
       <p style="font-size:10px;"><img src="http://cdn4.iconfinder.com/data/icons/picol/icons/phone_on_32.png"/>Escaneja el codi amb el teu mòbil per veure el llistat actualitzat</p>

       <h3>Poc transport públic? Vols estalviar benzina? Aquí tens algunes opcions més! Ajuda'ns a fer més gran aquest llistat!</h3>

       <div class="header-imprimible">Per compartir cotxe has de fer-te membre de la xarxa Eco Alt Congost al lloc web www.ecoaltcongost.org o als punts d'informació del teu poble! També pots escriure a info@ecoaltcongost.org si vols més informació.<br />
       </div>


       <?php
       	$viewName = 'print_views';
       	print views_embed_view('print_views', 'page_2');
       	?>
       	<div style="margin:20px 0px;"></div>
       <div class="header-imprimible">L'ECO Alt Congost és una comunitat d'intercanvi de béns i serveis amb una moneda local complementària interna que funciona com un sistema de punts. Hi poden participar els veïns i les veïnes dels pobles de l'Alt Congost, així com comerços locals, entitats i ajuntaments. Els objectius principals són potenciar el veïnatge, potenciar el comerç local, augmentar el poder adquisitiu dels participants, crear oportunitats laborals cooperatives i fomentar pràctiques que ajudin a preservar l'entorn dels nostres pobles i apostin per la vertadera sostenibilitat.</div>


  	</div>

    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif;?>

    <?php if (!empty($content['links'])): ?>
	    <div class="links"><?php print render($content['links']); ?></div>
	  <?php endif; ?>

	</div> <!-- /node-inner -->
</div> <!-- /node-->

<?php print render($content['comments']); ?>