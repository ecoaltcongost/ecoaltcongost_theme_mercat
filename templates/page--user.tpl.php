<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ HEADER _______________________ -->

<div id="topbar"> </div>

	<div id="headermenu">

	<div class="header-menu-inner">
	<div id="logo"><a href="<?php print $GLOBALS['base_url']; ?>/"><img src="<?php print $logo; ?>"/></a></div>

	<div id="menu-usuari">
	<?php global $user; ?>
	<?php if ($user->uid) : ?>
		<div id="credits-user"><?php print render($page['saldo']); ?> </div>
	<?php endif; ?>
		<div id="id-user"><?php print render($page['userid']); ?></div>
		<div id="options-user">
		<?php print render($page['menumembre']); ?>
		</div>

	</div> <!--menu usuari-->
	</div>
	</div>
</div>



<div id="header_message">
	<div id="header_message_inner">
	<?php print render($page['menuprincipal']); ?>
	</div>
</div>
<div id="main-content">
	<div id="main-content-inner">

		<div id="main-top">

		<?php if ($page['sidebar_first']): ?>
		<div id="sidebar">
			<div class="sidebar-inner">
			<?php print render($page['sidebar_first']); ?>
			<div class="social-profile"><?php print views_embed_view('social_networks_profile', 'default'); ?></div>
			<div class="button-recomanar"><?php print flag_create_link('recomanacio_membre', arg(1)); ?> <?php print views_embed_view('recomanacions_membre', 'block_1'); ?></div>
			<div class="membres-recomanar"> <?php print views_embed_view('recomanacions_membre', 'block'); ?> </div>
			</div>
		</div>
		<?php endif; ?>

		<div id="content">
		<?php global $user; ?>
		<?php if ($user->uid) : ?>
		<h1><?php print render($page['content']['system_main']['field_nomicognoms']['#items'][0]['value']); ?> - <span style="text-transform:uppercase;"><?php print render($page['content']['system_main']['#account']->name); ?> - <span class="location"><?php print render($page['content']['system_main']['field_user_poblacio'][0]['#title']); ?></span> </h1>
		<div id="buttons-profile">
			<div class="buttons-profile-div-1">
			 <div class="buttons-profile-div-inner">
				<?php print l(t('Contactar'), 'messages/new/' . $page['content']['system_main']['#account']->uid, array('attributes' => array('class' => 'botocomprar'))); ?>
				<?php if ($page['content']['system_main']['field_privacity_telefon']['#items'][0]['value']['0']): ?>

				<div class="profile-telefon"><?php print t('Telèfon no visible'); ?></div>

				<?php else : ?>
				<div class="profile-telefon"><?php print render($page['content']['system_main']['field_user_telefon']['#items'][0]['value']); ?> </div>
				<div class="profile-telefon"><?php print render($page['content']['system_main']['field_user_telefon']['#items'][1]['value']); ?> </div>
				<?php endif; ?>
			  </div>
			</div>

		<div class="buttons-profile-div-2">
		 <div class="buttons-profile-div-inner">
			<div class="profile-saldo"><?php print views_embed_view('user_transactions_summaries', 'block_1'); ?></div>
		 </div>
		</div>


		<div class="buttons-profile-div-4">
			<div class="buttons-profile-div-inner">
				<div class="profile-exchanges"><?php print views_embed_view('user_transactions_summaries', 'block_2'); ?></div>
			</div>
		</div>
		</div>
		<?php else : ?>
		<h1><?php print $title; ?></h1>
		 <?php endif; ?>


		<?php print render($page['content']); ?>



		</div>

	</div>
</div>


<div id="pre-footer">



</div>

<div id="footer">
<div id="menucenterpage">

	<?php print render($page['menufooter']); ?>

</div> <!-- menu centerpage -->

<div id="footer-inner"> <ul><li><a href="#">Text Legal</a></li><a href="#">Normes de funcionament</a><li></li></ul>
</div>


</div> <!-- /page -->
