<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>">
	<div class="node-inner">

    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>

    <?php print $user_picture; ?>

    <?php if ($display_submitted): ?>
      <span class="submitted"><?php print $date; ?> — <?php print $name; ?></span>
    <?php endif; ?>

  	<div class="content">
  	  <?php
  	    // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        print render($content);
       ?>

       <div class="header-imprimible">No ens queden Euros, però tenim Ecos! Per accedir a aquests productes i serveis has de fer-te membre de la xarxa Eco Alt Congost al lloc web www.ecoaltcongost.org o als punt d'informació del teu poble! També pots escriure a info@ecoaltcongost.org si vols més informació.</div>

       <h2>Serveis i coneixements</h2>
        <?php
       	$viewName = 'print_views';
       	print views_embed_view('print_views', 'page');
       	?>

       	<h2>Productes i eines</h2>

       <?php
       	$viewName = 'print_views';
       	print views_embed_view('print_views', 'page_1');
       	?>


       <h2>Espais i allotjament</h2>

       <?php
       	$viewName = 'print_views';
       	print views_embed_view('print_views', 'page_3');
       	?>

       	<h2>Compartir cotxe</h2>

       <?php
       	$viewName = 'print_views';
       	print views_embed_view('print_views', 'page_2');
       	?>


       	<h2>Comerços i professionals que accepten Ecos</h2>

       <?php
       	$viewName = 'print_views';
       	print views_embed_view('print_views', 'page_4');
       	?>

       <h2>Demandes</h2>

       <?php
       	$viewName = 'print_views';
       	print views_embed_view('print_views', 'page_5');
       	?>



  	</div>

    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif;?>

    <?php if (!empty($content['links'])): ?>
	    <div class="links"><?php print render($content['links']); ?></div>
	  <?php endif; ?>

	</div> <!-- /node-inner -->
</div> <!-- /node-->

<?php print render($content['comments']); ?>