<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ HEADER _______________________ -->

<div id="topbar"> </div>

	<div id="headermenu">

	<div class="header-menu-inner">
	<div id="logo"><a href="<?php print $GLOBALS['base_url']; ?>/"><img src="<?php print $logo; ?>"/></a></div>

	<div id="menu-usuari">
	<?php global $user; ?>
	<?php if ($user->uid) : ?>
		<div id="credits-user"><?php print render($page['saldo']); ?> </div>
	<?php endif; ?>
		<div id="id-user"><?php print render($page['userid']); ?></div>
		<div id="options-user">
		<?php print render($page['menumembre']); ?>
		</div>

	</div> <!--menu usuari-->
	</div>
	</div>
</div>



<div id="header_message">
	<div id="header_message_inner">
	<?php print render($page['menuprincipal']); ?>
	</div>
</div>
<div id="main-content">
	<div id="main-content-inner">

		<div id="main-top">
		      <?php if ($tabs): ?>
        <div class="tabs">
		      	<div class="tabs">
		      	<ul class="tabs primary clearfix"><li><a href="<?php print $GLOBALS['base_url']; ?>/user/me"><span class="tab">Mostrar perfil</span></a></li><li><a href="<?php print $GLOBALS['base_url']; ?>/user/me/edit"><span class="tab">Editar perfil</span></a></li><li><a href="<?php print $GLOBALS['base_url']; ?>/user/me/mailchimp"><span class="tab">Subscripció als butlletins</span></a></li></ul>
		      	</div>
        </div>
      <?php endif; ?>
		<div id="content">
		<?php print $messages; ?>
		<div id="content-add">
			<div class="content-form">
			<h2>Edita el teu compte</h2>
			<p class="description-add">

			</p>
			<?php print render($page['content']); ?>
			</div>
		</div>
	        </div>
	</div>
</div>



<div id="pre-footer">



</div>

<div id="footer">
<div id="footer-inner"> <ul><li><a href="#">Text Legal</a></li><a href="#">Normes de funcionament</a><li></li></ul>
</div>







</div> <!-- /page -->