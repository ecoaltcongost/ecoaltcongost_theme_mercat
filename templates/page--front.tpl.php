<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ HEADER _______________________ -->

<div id="topbar"> </div>

	<div id="headermenu">

	<div class="header-menu-inner">
	<div id="logo"><a href="<?php print $GLOBALS['base_url']; ?>/"><img src="<?php print $logo; ?>"/></a></div>

	<div id="menu-usuari">
	<?php global $user; ?>
	<?php if ($user->uid) : ?>
		<div id="credits-user"><?php print render($page['saldo']); ?> </div>

		<div id="id-user"><?php print render($page['userid']); ?></div>
		<div id="options-user">
		<?php print render($page['menumembre']); ?>
		</div>

	</div> <!--menu usuari-->

        <?php else : ?>

	<a id="botoaccedeix" href="<?php print $GLOBALS['base_url']; ?>/user/login">Accedeix</a> <a id="botoaccedeix" href="http://ecoaltcongost.org/inscripcions">Registra't!</a>
	<?php endif; ?>
	</div>
	</div>
</div>


<div id="header_message">
	<div id="header_message_inner">
	<?php print render($page['menuprincipal']); ?>
	</div>
</div>


<div id="main-content">

	<div id="main-content-inner">
	    <div id="main-top">
		<div id="main-left">
			<div class="main-inner">
			<?php print $messages; ?>
				<div id="container-items">
				<?php print render($page['container']); ?>
				</div>
			</div>
		</div>

	<?php global $user; ?>
	<?php if ($user->uid) : ?>
		<div id="main-right">
			<div class="main-inner no-padding-right">
				<h2>Et pot ser útil</h2>
				<div id="box-recomana"><div class="recomana-inner">
				<ul>
				<li><a href="http://www.appsgeyser.com/getwidget/ecoaltcongost/" target="_blank">Aplicació mòbil</a></li>
				<li><a href="<?php print $GLOBALS['base_url']; ?>/sites/default/files/xecs.pdf">Imprimir xecs</a></li>
				<li><a href="<?php print $GLOBALS['base_url']; ?>/sites/default/files/contracteprestec.pdf">Contracte de préstec d'ús</a></li>
				<li><a href="<?php print $GLOBALS['base_url']; ?>/faq">Preguntes freqüents</a></li>
				</ul>
				</div></div>
				</div>

			<div class="main-inner no-padding-right">
			<div class="infoadicional">
			<span class="needhelp">Necessites ajuda?</span><br />
			<span class="telefon">668 872 873 (18h - 20h)</span>
			<span class="mail">info@ecoaltcongost.org</span>
			</div>

			<div class="donations">
			<b>Fes créixer la xarxa!</b><br />
			<span class="donations-text">Parla de l'ECO Alt Congost als teus amics i coneguts.</span>

			</div>

			</div>
	    </div>

	    <?php else : ?>


	    <div id="main-right">
			<div class="main-inner no-padding-right">

				<h2>Registra't a l'Eco Alt Congost!</h2>
				<div id="box-recomana"><div class="recomana-inner">
				<ul>
				<li><a href="http://ecoaltcongost.org/inscripcions" target="_blank">Formulari d'alta</a></li>
				<li><a href="http://ecoaltcongost.org/faq" target="_blank">Preguntes freqüents</a></li>
				</ul>
				</div></div>
			</div>

			<div class="main-inner no-padding-right">
			<div class="infoadicional">
			<span class="needhelp">Necessites ajuda?</span><br />
			<span class="mail">info@ecoaltcongost.org</span>
			</div>

			<div class="donations">
			<b>Fes créixer la xarxa!</b><br />
			<span class="donations-text">Ajuda als teus veïns oferint els teus coneixements i serveis o presta aquelles coses que no utilitzes sovint. Guanyaràs ecos a canvi que podràs gastar en allò que t'interessa dins de la xarxa. </span>

			</div>

			</div>
	    </div>


	    <?php endif; ?>

	    <div id="centerpage">
	    <div class="centerpage-inner"> <?php print render($page['centerpage']); ?> </div>
	    </div>

		</div>
	    </div>

         </div></div>
</div>



<div id="footer">

	<div id="menucenterpage">

		<div class="menus-front">

	    <div style="padding-left:10px;padding-top:5px;"></div>

	    <div id="columna-llistat">
	    	<div class="columna-llistat-inner">
	    	<?php print render($page['menuserveis']); ?>
	    	</div>
	    </div>

	    <div id="columna-llistat">
	    	<div class="columna-llistat-inner">
	    	<?php print render($page['menuproductes']); ?>
	    	</div>
	    </div>


    	    <div id="columna-llistat">
	    	<div class="columna-llistat-inner">
	    	<h2>Comerços</h2>
	    	<div class="item-list">
	    	<ul>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=88">Aiguafreda</a></li>
	        <li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=93">Balenyà</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=92">Centelles</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=89">Figaró-Montmany</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=89">La Garriga</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=90">Sant Martí de Centelles</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=94">Seva</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=91">Tagamanent</a></li>
	    	</ul>
	    	</div>
	    	<?php print render($page['menuespais']); ?>
	    	</div>
	    </div>


	    <div id="columna-llistat">
	    	<div class="columna-llistat-inner">
	    	<h2>Compartir cotxe des de</h2>
	    	<div class="item-list">
	    	<ul>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=88">Aiguafreda</a></li>
	        <li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=93">Balenyà</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=92">Centelles</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=89">Figaró-Montmany</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=89">La Garriga</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=90">Sant Martí de Centelles</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=94">Seva</a></li>
	    	<li><a href="<?php print $GLOBALS['base_url']; ?>/ofertes/compartircotxe?field_taxonomy_origen_tid=91">Tagamanent</a></li>
	    	</ul>
	    	</div>


	    	<?php print render($page['menuterres']); ?>

	    	</div>
	    </div>

	    </div>

	</div> <!-- menu centerpage -->








</div> <!-- /page -->