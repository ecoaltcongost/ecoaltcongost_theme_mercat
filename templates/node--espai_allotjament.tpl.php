<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>">
	<div class="node-inner">

	<div id="image-ad">
	<?php print render($content['field_image']); ?>
	</div>

	<div id="description-ad">
	<h1>Ofertes > <a href="<?php print $GLOBALS['base_url']; ?>/ofertes/productes">Espais i allotjament</a> > <a href="<?php print $GLOBALS['base_url']; ?>/ofertes/allotjamentiespais?categoria=<?php print $node->field_espai_taxonomy['und'][0]['taxonomy_term']->tid; ?>">
	<?php print $node->field_espai_taxonomy['und'][0]['taxonomy_term']->name; ?>
	</a></h1>
	<h2><?php print $title; ?></h2>




	<b>Descripció</b>

	<?php print render($content['field_description']); ?>
	</div>

	<div id="created-ad">
	Data de publicació: <?php print date("d/m/y G.i", $created) ?> <br/>
	Oferta vàlida fins el: <?php print $node->scheduler['unpublished']; ?>
	</div>

		<div id="preuanunci">
	<div class="preuanunci-inner">
	<?php global $user; ?>
	<?php if ($user->uid) : ?>

	<div id="contacte-caixa">

	<div class="telefon">


	<?php
	$node_author = user_load($node->uid);

	if ($node_author->field_privacity_telefon['und'][0]['value']): ?>

	<?php print t('No visible'); ?>

	<?php else : ?>

	<?php
	$node_author = user_load($node->uid);
		print ($node_author->field_user_telefon['und'][0]['value']);
	?>

	<?php endif; ?>
	</div>

	<?php
		if ($url = privatemsg_get_link(array(user_load($node->uid)))) {
			print l(t('Contactar'), $url, array('attributes' => array('class' => 'botocomprar')));
		}
	?>

	</div>



	<?php endif; ?>
	<div><div class="importeco"><?php print $node->field_ecos['und'][0]['value']; ?> ECO</div></div>
	<?php if ($content['field_price_periode']) : ?>
        <span style="float:left; margin-right:5px;">Preu per:</span> <?php print render($content['field_price_periode']); ?>
        <?php endif; ?>
        Ref: #<?php print $nid; ?>
	</div>
	</div>


		<div id="comentaris">
		<?php print render($content['comments']); ?>
	</div>


	</div> <!-- /node-inner -->
</div> <!-- /node-->
