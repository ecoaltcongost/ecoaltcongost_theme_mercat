<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>


  <!-- ______________________ HEADER _______________________ -->

<div id="topbar"> </div>

	<div id="headermenu">

	<div class="header-menu-inner">
	<div id="logo"><a href="<?php print $GLOBALS['base_url']; ?>/"><img src="<?php print $logo; ?>"/></a><span class="logotroquescola"><a href="<?php print $GLOBALS['base_url']; ?>/troquescola"><img src="/sites/all/themes/basic/css/images/logotroquescola.png"/></span></a></div>

	<div id="menu-usuari">
	<?php global $user; ?>
	<?php if ($user->uid) : ?>
		<div id="credits-user"><?php print render($page['saldo']); ?> </div>

		<div id="id-user"><?php print render($page['userid']); ?></div>
		<div id="options-user">
		<?php print render($page['menumembre']); ?>
		</div>

	</div> <!--menu usuari-->

        <?php else : ?>

	<a id="botoaccedeix" href="<?php print $GLOBALS['base_url']; ?>/user/login">Accedeix</a>
	<?php endif; ?>
	</div>
	</div>
</div>



<div id="header_message">
	<div id="header_message_inner">
	<?php print render($page['menuprincipal']); ?>
	</div>
</div>


<div id="main-content">
	<div id="main-content-inner">

		<div id="main-top">

		<?php if ($page['sidebar_first']): ?>
		<div id="sidebar">
			<div class="sidebar-inner">
			<?php print render($page['sidebar_first']); ?>
			</div>
		</div>
		<?php endif; ?>

		<div id="content">
		<h1><?php print $title;?></h1>
		<?php print $messages; ?>

			<?php print render($page['content']); ?>
		</div>

	</div>

      	    <div id="centerpage">
	    <div class="centerpage-inner"> <?php print render($page['centerpage']); ?> </div>
	    </div>

</div>

<?php global $user; ?>
<?php if ($user->uid) : ?>

<div id="pre-footer">


</div>
<?php endif; ?>
<div id="footer">
<div id="footer-inner"> <ul><li><a href="#">Text Legal</a></li><a href="#">Normes de funcionament</a><li></li></ul>
</div>







</div> <!-- /page -->