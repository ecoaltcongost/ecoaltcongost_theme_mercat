<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ HEADER _______________________ -->

<div id="topbar"> </div>

	<div id="headermenu">

	<div class="header-menu-inner">
	<div id="logo"><a href="<?php print $GLOBALS['base_url']; ?>/"><img src="<?php print $logo; ?>"/></a><span class="logotroquescola"><a href="<?php print $GLOBALS['base_url']; ?>/troquescola"><img src="/sites/all/themes/basic/css/images/logotroquescola.png"/></span></a></div>

	<div id="menu-usuari">
	<?php global $user; ?>
	<?php if ($user->uid) : ?>
		<div id="credits-user"><?php print render($page['saldo']); ?> </div>

		<div id="id-user"><?php print render($page['userid']); ?></div>
		<div id="options-user">
		<?php print render($page['menumembre']); ?>
		</div>

	</div> <!--menu usuari-->

        <?php else : ?>

	<a id="botoaccedeix" href="<?php print $GLOBALS['base_url']; ?>/user/login">Accedeix</a>
	<?php endif; ?>
	</div>
	</div>
</div>


<div id="header_message">
	<div id="header_message_inner">
	<?php print render($page['menuprincipal']); ?>
	</div>
</div>


<div id="box-front">
<div id="box-front-width">
<div id="slider">
<div class="box-front-inner"> <ul><li></li>
</ul></div>
</div>
</div>
</div>

<div id="main-content">

	<div id="main-content-inner">
	    <div id="main-top">
		<div id="main-left">
			<div class="main-inner">
			<?php print $messages; ?>
				<div id="container-items">
				<h2>Properes classes</h2>
				<?php print render($page['container']); ?>
				<?php print render($page['content']); ?>
				</div>
			</div>
		</div>

	<?php global $user; ?>
	<?php if ($user->uid) : ?>
			    <div id="main-right">
			<div class="main-inner no-padding-right">
				<h2>Aprèn i ensenya!</h2>
				<div class="donations">
				<b>Activitats obertes a tothom!</b><br />
				<span class="donations-text">Ensenya aquelles coses que t'agraden a canvi d'Ecos, béns, serveis i/o coneixements!</span>

				</div>
			</div>

			<div class="main-inner no-padding-right">
			<div class="infoadicional">
			<span class="needhelp">Necessites ajuda?</span><br />
			<span class="mail">info@ecoaltcongost.org</span>
			</div>

			<div class="donations">
			<b>Fes créixer la xarxa!</b><br />
			<span class="donations-text">Ajuda als teus veïns oferint els teus coneixements i serveis o presta aquelles coses que no utilitzes sovint. Guanyaràs ecos a canvi que podràs gastar en allò que t'interessa dins de la xarxa. </span>

			</div>

			</div>
	    </div>

	    <?php else : ?>


	    <div id="main-right">
			<div class="main-inner no-padding-right">
				<h2>Aprèn i ensenya!</h2>
				<div class="donations">
				<b>Activitats obertes a tothom!</b><br />
				<span class="donations-text">Encara que no siguis membre de la xarxa Eco Alt Congost, pots aprendre i participar a les activitats publicades.</span>

				</div>
			</div>

			<div class="main-inner no-padding-right">
			<div class="infoadicional">
			<span class="needhelp">Necessites ajuda?</span><br />
			<span class="mail">info@ecoaltcongost.org</span>
			</div>

			<div class="donations">
			<b>Fes créixer la xarxa!</b><br />
			<span class="donations-text">Ajuda als teus veïns oferint els teus coneixements i serveis o presta aquelles coses que no utilitzes sovint. Guanyaràs ecos a canvi que podràs gastar en allò que t'interessa dins de la xarxa. </span>

			</div>

			</div>
	    </div>


	    <?php endif; ?>



		</div>
	    </div>

         </div></div>

               	    <div id="centerpage">
	    <div class="centerpage-inner"> <?php print render($page['centerpage']); ?> </div>
	    </div>
</div>



<div id="footer">




</div>


</div> <!-- /page -->